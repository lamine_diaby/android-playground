package com.example.administrateur.orsys;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class AddClientActivity extends AppCompatActivity {

    private static final String TAG = "AddClientActivity";

    private EditText lastNameEditText;
    private EditText firstNameEditText;
    private Switch activeSwitch;
    private Spinner levelSpinner;
    private EditText emailEditText;
    private Calendar birthdate;
    private Button birthdateButton;
    private RadioGroup genderRadioGroup;
    private RadioButton manRadioButton;
    private RadioButton womanRadioButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_client);
        lastNameEditText = (EditText) findViewById(R.id.lastname_edittext);
        firstNameEditText = (EditText) findViewById(R.id.firstname_edittext);
        emailEditText = (EditText) findViewById(R.id.email);

        // Sexe.
        genderRadioGroup = (RadioGroup) findViewById(R.id.sexe_radiogroup);
        manRadioButton = (RadioButton) findViewById(R.id.man_radiobutton);
        womanRadioButton = (RadioButton) findViewById(R.id.femme_radiobutton);

        activeSwitch = (Switch) findViewById(R.id.active_switch);
        activeSwitch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Snackbar snackbar;
                snackbar = Snackbar
                        .make(findViewById(R.id.main_layout), "Le compte est activé", Snackbar.LENGTH_SHORT);

                View snackbarView = snackbar.getView();
                snackbarView.setBackgroundColor(Color.GRAY);
                TextView snackbarTextView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                if (isChecked) {
                    snackbar.show();
                    snackbarTextView.setTextColor(Color.GREEN);
                }
                else {
                    snackbar.show();
                    snackbar.setText("Le compte est désactivé");
                    snackbarTextView.setTextColor(Color.YELLOW);
                }
            }
        });

        levelSpinner = (Spinner) findViewById(R.id.level_spinner);
        ArrayAdapter<CharSequence> adapter =
                ArrayAdapter.createFromResource(this, R.array.levels, android.R.layout.simple_spinner_item);

        levelSpinner.setAdapter(adapter);

        birthdateButton = (Button) findViewById(R.id.birthdate_button);
    }

    public void onAddButtonClicked(View view) {
        Log.i(TAG, "onAddButtonClicked");
        Log.i(TAG, String.format("Nom : %s", lastNameEditText));

        String lastname = lastNameEditText.getText().toString();
        String firstname = firstNameEditText.getText().toString();
        String email = emailEditText.getText().toString();
        String level = levelSpinner.getSelectedItem().toString();
        boolean active = activeSwitch.isChecked();
        Client.Gender gender =
                genderRadioGroup.getCheckedRadioButtonId() == R.id.man_radiobutton ? Client.Gender.MAN : Client.Gender.WOMAN;

        Client client = new Client(lastname, firstname, email, birthdate.getTime(), level, gender, active);

        Client.getClients().add(client);

        Toast.makeText(this, String.format("Client %s ajouté", lastname), Toast.LENGTH_LONG).show();

        finish();
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch(view.getId()) {
            case R.id.man_radiobutton:
                if (checked) {
                    ((RadioButton) view).setTextColor(Color.BLUE);
                    RadioButton radioButtonSexeFemme = (RadioButton) findViewById(R.id.femme_radiobutton);
                    radioButtonSexeFemme.setTextColor(Color.BLACK);
                }

                break;
            case R.id.femme_radiobutton:
                if (checked) {
                    ((RadioButton) view).setTextColor(Color.RED);
                    RadioButton radioButtonSexeHomme = (RadioButton) findViewById(R.id.man_radiobutton);
                    radioButtonSexeHomme.setTextColor(Color.BLACK);
                }
                break;
        }
    }

    public void onSwitch(View view) {
        boolean active = activeSwitch.isChecked();
        Log.i(TAG, "Actif : " + active);
    }

    public void onDateSelect(View view) {
        if (birthdate == null) {
            birthdate = Calendar.getInstance();
        }
        int current_year = birthdate.get(Calendar.YEAR);
        int current_month = birthdate.get(Calendar.MONTH);
        int current_day = birthdate.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                birthdate.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat sdf =  new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH);
                String date = sdf.format(birthdate.getTime());
                birthdateButton.setText(date);
            }
        }, current_year, current_month, current_day);
        dialog.show();
    }
}
