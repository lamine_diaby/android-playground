package com.example.administrateur.orsys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class HomeActivity extends AppCompatActivity implements ListClientFragment.OnClientSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    @Override
    public void onClientSelected(int position) {
        if (findViewById(R.id.details_client_fragment) != null) {
            DetailsClientFragment detailsClientFragment =
                    (DetailsClientFragment) getSupportFragmentManager().findFragmentById(R.id.details_client_fragment);
            detailsClientFragment.updateClient(position);
        } else {
            Intent intent = new Intent(this, DetailsClientActivity.class);
            intent.putExtra(ListClientFragment.CLIENT_ID, position);
            startActivity(intent);
        }
    }
}
