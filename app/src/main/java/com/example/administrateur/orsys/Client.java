package com.example.administrateur.orsys;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Client {

    public enum Gender {
        MAN, WOMAN;
    }
    private String lastname;
    private String firstname;
    private String email;
    private Date birthdate;
    private String level;
    private Gender gender;
    private boolean active;

    private static List<Client> clients = new ArrayList<>();

    public Client() {
        this("", "", "", new Date(), "", Gender.MAN, true);
    }

    public Client(String lastname, String firstname, String email, Date birthdate, String level, Gender gender, boolean active) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.email = email;
        this.birthdate = birthdate;
        this.level = level;
        this.gender = gender;
        this.active = active;
    }

    public static List<Client> getClients() {
        return clients;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
