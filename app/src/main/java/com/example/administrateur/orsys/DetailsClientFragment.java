package com.example.administrateur.orsys;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class DetailsClientFragment extends Fragment {

    public static final int REQUEST_IMAGE_CAPTURE = 1;
    private TextView lastnameTextView;
    private TextView firstnameTextView;
    private TextView emailTextView;
    private TextView birthdateTextView;
    private TextView genderTextView;
    private Client client;
    private ImageView photoImageView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_details_client, container);

        // Initialisation des champs.
        lastnameTextView = (TextView) view.findViewById(R.id.lastname_textview);
        lastnameTextView = (TextView) view.findViewById(R.id.lastname_textview);
        firstnameTextView = (TextView) view.findViewById(R.id.firstname_textview);
        emailTextView = (TextView) view.findViewById(R.id.email_textview);
        birthdateTextView = (TextView) view.findViewById(R.id.birthdate_textview);
        genderTextView = (TextView) view.findViewById(R.id.gender_textview);

        photoImageView = (ImageView) view.findViewById(R.id.photo_imageview);
        photoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentTakePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intentTakePhoto, REQUEST_IMAGE_CAPTURE);
            }
        });

        // Mise à jour des informations du client.
        updateClient(0);
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            Bitmap bitmap = data.getParcelableExtra("data");
            FileOutputStream output = null;
            try {
                output = getActivity().openFileOutput("picture_" + client.getLastname(), Context.MODE_PRIVATE);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
                photoImageView.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void updateClient(int idClient) {
        client = Client.getClients().get(idClient);

        lastnameTextView.setText(client.getLastname());
        firstnameTextView.setText(client.getFirstname());
        emailTextView.setText(client.getEmail());
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH);
        String birthdate = sdf.format(client.getBirthdate().getTime());
        birthdateTextView.setText(birthdate);
        genderTextView.setText(client.getGender() == Client.Gender.MAN ? "Homme" : "Femme");

        try {
            FileInputStream inputStream =
                    getActivity().openFileInput("picture_" + client.getLastname());
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            photoImageView.setImageBitmap(bitmap);
        } catch (FileNotFoundException e) {
            photoImageView.setImageResource(
                    client.getGender() == Client.Gender.MAN ? R.drawable.man : R.drawable.woman);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.details_client, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_call_client:
                Uri uri_tel = Uri.parse("tel://00000000");
                Intent intentCall = new Intent(Intent.ACTION_DIAL, uri_tel);
                startActivity(intentCall);
                return true;
            case R.id.action_mail_client:
                Uri uri_mail = Uri.parse("mailto:" + client.getEmail());
                Intent intentMail = new Intent(Intent.ACTION_SENDTO, uri_mail);
                startActivity(intentMail);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
