package com.example.administrateur.orsys;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

public class ClientAdapter extends ArrayAdapter<Client> implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = "ClientAdapter";
    public static final String NAMES_ORDER_PREF_KEY = "names_order_pref";
    public static final String NAMES_ORDER_PREF_DEFAULT_VALUE = "LASTNAME_FIRSTNAME";
    private final SharedPreferences preferences;

    public ClientAdapter(Context context, List<Client> objects) {
        super(context, 0, objects);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        preferences.registerOnSharedPreferenceChangeListener(this) ;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        String name;
        String order = preferences.getString(NAMES_ORDER_PREF_KEY, NAMES_ORDER_PREF_DEFAULT_VALUE);

        Client client = getItem(position);
        Log.i(TAG, "getView: " + convertView);

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.adapter_client, parent, false);
        }

        TextView clientName = (TextView) convertView.findViewById(R.id.client_name);
        ImageView clientImage = (ImageView) convertView.findViewById(R.id.client_imageview);
        try {
            FileInputStream inputStream = getContext().openFileInput("picture_" + client.getLastname());
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            clientImage.setImageBitmap(bitmap);
        } catch (FileNotFoundException e) {
            clientImage.setImageResource(client.getGender() == Client.Gender.MAN ? R.drawable.man : R.drawable.woman);
        }

        if (NAMES_ORDER_PREF_DEFAULT_VALUE.equals(order)) {
            name = client.getLastname() + " " + client.getFirstname();
        } else {
            name = client.getFirstname() + " " + client.getLastname();
        }
        clientName.setText(name);
        return convertView;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (NAMES_ORDER_PREF_KEY.equals(key)) {

        }
    }
}
