package com.example.administrateur.orsys;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;

public class ListClientFragment extends ListFragment {

    private static final String TAG = "ListClientFragment";
    public static final String CLIENT_ID = "id";
    public static final String CLIENT_SYNCHRO_FINISH = "CLIENT_SYNCHRO_FINISH";
    private ArrayAdapter<Client> adapter;

    public interface OnClientSelectedListener {
        void onClientSelected(int position);
    }

    private OnClientSelectedListener listener;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            adapter.notifyDataSetChanged();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new ClientAdapter(getContext(), Client.getClients());

        setHasOptionsMenu(true);
        setListAdapter(adapter);

        // Réception du broadcast en fin de synchro.
        IntentFilter filter = new IntentFilter(CLIENT_SYNCHRO_FINISH);
        getActivity().registerReceiver(receiver, filter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (listener != null) {
            listener.onClientSelected(position);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.list_client, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_client:
                Intent intent = new Intent(getContext(), AddClientActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_synchro:
                synchro();
                return true;
            case R.id.action_settings:
                Intent intentSettings = new Intent(getContext(), SettingsActivity.class);
                startActivity(intentSettings);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void synchro() {

        // Pour éviter les erreurs de type :
        // NetworkOnMainThreadException, on fait une tâche asynchrone.
        AsyncTask<String, Void, Void> task = new AsyncTask<String, Void, Void>() {

            @Override
            protected void onPostExecute(Void aVoid) {
                NotificationCompat.Builder builder = new NotificationCompat.Builder(getActivity());
                builder.setContentTitle("Synchronisation");
                builder.setContentText("Synchronisation terminée");
                builder.setWhen(new Date().getTime());
                builder.setSmallIcon(android.R.drawable.stat_notify_sync);

                Notification notification = builder.build();

                NotificationManager notificationManager =
                        (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(2121982, notification);
            }

            @Override
            protected Void doInBackground(String... params) {
                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    InputStream inputStream = connection.getInputStream();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    String json = reader.readLine();
                    JSONArray jsonArray = new JSONArray(json);

                    List<Client> clients = JSONUtils.parse(jsonArray);
                    Client client = clients.get(0);
                    Client.getClients().add(client);
                    Log.d(TAG, "synchro: " + json);

                    Intent intent = new Intent(CLIENT_SYNCHRO_FINISH);
                    getActivity().sendBroadcast(intent);
                } catch (Exception e) {
                    Log.e(TAG, "synchro: ", e);
                }
                return null;
            }
        };
        task.execute("http://ama-gestion-clients.appspot.com/rest/client");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getActivity() instanceof OnClientSelectedListener) {
            listener  = (OnClientSelectedListener) getActivity();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(receiver);
    }
}
