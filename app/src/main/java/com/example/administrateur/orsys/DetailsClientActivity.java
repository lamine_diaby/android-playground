package com.example.administrateur.orsys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class DetailsClientActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_client);

        Intent intent = getIntent();
        int idClient = intent.getIntExtra(ListClientFragment.CLIENT_ID, 0);

        DetailsClientFragment detailClientFragment =
                (DetailsClientFragment) getSupportFragmentManager().findFragmentById(R.id.details_client_fragment);

        detailClientFragment.updateClient(idClient);
    }
}
